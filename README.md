docker samba
============

## Quickstart

### Getting Default Config File

```bash
docker create itsdaspecialk/samba
docker cp $(docker ps -ql):/etc/samba/smb.conf .
docker rm $(docker ps -ql)
```

**NOTE:** Make sure to use `passdb backend = tdbsam` in the config file, unless you know what you are doing.

#### Getting a more detailed example Config File

```bash
docker create itsdaspecialk/samba
docker cp $(docker ps -ql):/etc/samba/smb.conf.example .
docker rm $(docker ps -ql)
```

### Get help for config file
[smb.conf man page](https://linux.die.net/man/5/smb.conf)

## Start the container 

```bash
docker run -d --name samba \
    -p 137:137/udp -p 138:138/udp -p 139:139 -p 445:445 \
    -v <path/to/share>:<path/to/share> \
    -v samba_private:/var/lib/samba \
    -v samba_group:/etc/group \
    -v samba_passwd:/etc/passwd \
    -v samba_shadow:/etc/shadow \
    itsdaspecialk/samba
```

This creates a named volume to store the samba tdb files. If you would rather specify a location for it, replace `samba_private` with a path above.
Any arguments passed to the container get passed to both smbd **AND** nmbd. If you would rather only pass parameters to one of the two commands, use the environment variables `NARGS` and `SARGS` for nmbd and smbd respectively.

### Alternate Run Instructions

#### Use host users and groups

You can use the host groups and users in the samba container, using the following command:

```bash
docker run -d ... -v samba_private:/var/lib/samba \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/shadow:/etc/shadow:ro \
    itsdaspecialk/samba
```

If using this method, when adding/modifying users and groups, do so on your host system. You will, however, still need to use `smbpasswd -a`

**NOTE:** I have only tried this in Redhat and Centos! Theoretically, it should work on other distros too, as long as there is are `root` and `nobody` users and groups
**NOTE 2:** I highly recommend using the read-only mode, if you choose to use this method.

## Adding users or groups

**NOTE:** The container must be running for these steps.

### Adding users

For users, Samba uses its own `passdb.tdb` file to authenticate user passwords, but still uses the standard UNIX user files.
If you are not using the host system `/etc/passwd` file, you first need to create a user using the following command:

```bash
docker exec -it <container_name/hash> useradd -M <username>
```

Then set the password for samba.

```bash
docker exec -it <container_name/hash> smbpasswd -a <username>
```

### Adding groups

First, create the group:

```bash
docker exec -it <container_name/hash> groupadd <groupname>
```

Next, add a user to the group:

```bash
docker exec -it <container_name/hash> usermod -a -G <groupname> <username>
```

## Issues

If you have an issue, please create an [issue on Bitbucket](https://bitbucket.org/ItsDaSpecialK/docker-samba/issues)
