#!/bin/sh

set -e -u -o pipefail

mkdir -p /var/lib/samba/drivers /var/lib/samba/lock /var/lib/samba/private

nmbd -D "$@" "${NARGS:-""}"
exec smbd -FS "$@" "${SARGS:-""}" </dev/null
